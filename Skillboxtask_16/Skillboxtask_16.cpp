﻿#include <iostream>
#include <time.h>


int main()
{


    //Задача 1. Двумерный массив

        const int size = 4;
        int array[size][size] = 
        { 
            {0, 1, 2, 3}, 
            {1, 2, 3, 4}, 
            {2, 3, 4, 5}, 
            {3, 4, 5, 6} 
        };

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                std::cout << array[i][j] << "\t";
            }
            std::cout << '\n';
        }

    //Задача 2. Суммирование строк массива

        struct tm buf;
        time_t t = time(NULL);
        localtime_s(&buf, &t);
        std::cout << "\n" << "Current day is: " << buf.tm_mday << "\n";
        int Index = buf.tm_mday % size; 
        std::cout << "\n" << "Index of the row is: " << Index; //индекс строки массива

        int sum = 0;

        for (int i = Index; i <= Index; i++)
        {
            for (int j = 0; j < size; j++)
            {
                sum += array[i][j];
            }
            std::cout << '\n' << "Sum of the row is: " << sum << '\n';
        }


        return 0;

}

